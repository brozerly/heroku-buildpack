#!/bin/sh
# Add an MAIL_URL using Mandrill credentials.
echo "-----> Adding MAIL_URL env"
cat > "$APP_CHECKOUT_DIR"/.profile.d/mandrill.sh <<EOF
  #!/bin/sh
  # Set MAIL_URL if it's not already set
  if [ -z \$MAIL_URL ] ; then
    USERNAME=\$(echo \$MANDRILL_USERNAME | sed -e 's/@/%40/g')
    export MAIL_URL=smtp://\$USERNAME:\$MANDRILL_APIKEY@smtp.mandrillapp.com:587
  fi
EOF