#!/bin/sh
# Add an METEOR_SETTINGS depending the env.
echo "-----> Adding METEOR_SETTINGS env"
cat > "$APP_CHECKOUT_DIR"/.profile.d/meteor_settings.sh <<EOF
  #!/bin/sh
  # Set METEOR_SETTINGS
  export METEOR_SETTINGS="\$(cat ~/config/\${ENV}_settings.json)" 
EOF